import React, { Component } from "react";
import { data_glasses } from "./data.js"
export default class Glassees extends Component {
    state = {
        glassesCurrent: {
            "id": 1,
            "price": 30,
            "name": "GUCCI G8850U",
            "url": "./glassesImage/v1.png",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        }
    }
    renderGlasses = () => {
        return data_glasses.map((item) => {
            return <img onClick={() => { this.changeGlasses(item) }} className="ml-3 p-2 border border-width-1" style={{ width: "80px", cursor: "pointer" }} src={item.url} alt="" />

        })

    }
    changeGlasses = (newGlasses) => {
        this.setState({
            glassesCurrent: newGlasses
        })
    }
    render() {
        const keyFrame = `@keyframes animateGlasses${Date.now()} {
            from {
                
                opacity:0
                
            }to{
               
                opacity:0.7
                
            }}
            `
        const styleGlasses = {
            width: "88px",
            top: "46px",
            right: "110px",
            opacity: 0.7,
            animation: `animateGlasses${Date.now()} 1.5s`
        }
        const infoGlasses = {
            width: "150px",
            left: "230px",
            top: "119px",
            textAlign: "left",
            backgroundColor: "rgba(255,127,0,.5)",
            height: "64px",
            paddingLeft: "10px",
            lineHeight: 1
        }
        return (
            <div
                style={{
                    backgroundImage: `url(./glassesImage/background.jpg)`,
                    minHeight: `600px`,
                    backgroundSize: `cover`,
                }}
            >
                <style>{keyFrame}</style>
                <div style={{ backgroundColor: `rgba(0,0,0,.8)`, minHeight: `650px` }}>
                    <h3
                        style={{ backgroundColor: `rgba(0,0,0,.3)` }}
                        className="text-center text-light p-3"
                    >
                        TRY GLASSES APP ONLINE
                    </h3>
                    <div className="container">
                        <div className="row mt-5 px-5">
                            <div className="col-6 pl-5 ">
                                <div className="position-relative">
                                    <img className="position-absolute"
                                        style={{ width: `150px` }}
                                        src="./glassesImage/model.jpg"
                                        alt=""
                                    />
                                    <img style={styleGlasses} className="position-absolute" src={this.state.glassesCurrent.url} alt="" />
                                    <div style={infoGlasses} className="position-relative">
                                        <span style={{ color: "#AB82FF", fontWeight: "bold", fontSize: "13px" }} className="mb-0 ">{this.state.glassesCurrent.name}</span><br />
                                        <span style={{ fontWeight: "800", fontSize: "6px", lineHeight: "1" }} className="mb-0">{this.state.glassesCurrent.desc}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-6 pr-5">
                                <img
                                    style={{ width: `150px` }}
                                    src="./glassesImage/model.jpg"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                    <div style={{ padding: "0 50px" }} className="bg-light container mt-5 py-2 d-flex justify-content-center ">{this.renderGlasses()}</div>
                </div>
            </div>
        );
    }
}

